#!/usr/bin/env python
from bs4 import BeautifulSoup
import urllib2
import time
import subprocess
import numpy as np
import sys

# Ugly hack so git submodule init is adequate.
sys.path.insert(0, './btce-api/')
import btceapi
sys.path.insert(0, './pyvircurex/')
import vircurex as vircurexapi
sys.path.insert(0, './PyCryptsy/')
from PyCryptsy import PyCryptsy
sys.path.insert(0, './python-bittrex/')
import bittrex

import ConfigParser
import simplejson
import socket
import requests

# -----------
# Hopefully nothing below this needs editing.
# -----------

class Coin:
    def __init__(self, name):
        self.ratio = 0 # assume totally unprofitable unless otherwise shown to be the case.
        self.willingToMine = False
        self.miningNow = False
        self.merged = False
        self.willingToSell = False
        self.command = '' # the command that is run when we want to mine this coin.
        self.name = name
        self.cnt = 0
        self.median = 0
        self.m = 0
        self.h = 0
        self.fee = 0
        self.source = '--'
        self.price = 0.0
        self.diff = 9999999999.9 # start off with ridiculously high diff so we dont mine the wrong coin
        self.reward = 0
        self.algo = ""

coins = {}      # Creating an empty dictionary for the coins
diffsrc = {}    # Creating an empty dictionary for btg and trc
coins['alf_scrypt'] =  Coin('AlphaCoin')
coins['amc_scrypt'] =  Coin('AmericanCoin')
coins['anc_scrypt'] =  Coin('Anoncoin')
coins['arg_scrypt'] =  Coin('Argentum')
coins['asc_sha-256'] =  Coin('Asiccoin')
coins['aur_scrypt'] =  Coin('AuroraCoin')
coins['bet_sha-256'] =  Coin('Betacoin')
coins['bob_scrypt'] =  Coin('Dobbscoin')
coins['bfc_sha-256'] =  Coin('ButterflyCoin')
coins['bqc_scrypt'] =  Coin('BBQCoin')
coins['btb_scrypt'] =  Coin('Bitbar')
coins['btc_sha-256'] =  Coin('Bitcoin')
coins['bte_cryptonight'] =  Coin('Bytecoin')
coins['btg_scrypt'] =  Coin('Bitgem')
diffsrc['btg_scrypt'] = 'http://bitgem.dyndns.org:2750/chain/Bitgem/q/getdifficulty'
coins['cap_scrypt'] =  Coin('Bottlecap')
coins['ckc_x11'] =  Coin('Checkcoin')
coins['cmc_scrypt'] =  Coin('Cosmoscoin')
coins['child_x11'] =  Coin('Childcoin')
coins['cgb_scrypt'] =  Coin('CryptogenicBullion')
coins['cbx_scrypt'] =  Coin('CryptoBullion')
coins['cnc_scrypt'] =  Coin('CHNCoin')
coins['crc_scrypt'] =  Coin('Craftcoin')
coins['dash_x11'] =  Coin('Dash')
coins['dbl_scrypt'] =  Coin('Doubloons')
coins['dem_sha-256'] =  Coin('eMark')
coins['dmd_groestl'] =  Coin('Diamond')
coins['dgb_skein'] = Coin('DigitalbyteSkein')
coins['dgb_groestl'] = Coin('DigitalbyteGroestl')
coins['dgb_qubit'] = Coin('DigitalbyteQubit')
coins['dgb_scrypt'] = Coin('DigitalbyteScrypt')
coins['dgc_x11'] =  Coin('DigitalCoinX11')
coins['dgc_scrypt'] =  Coin('DigitalCoinScrypt')
coins['doge_scrypt'] = Coin('DogeCoinScrypt')
coins['eac_scrypt'] =  Coin('EarthcoinScrypt')
coins['emc2_scrypt'] =  Coin('Einstenium')
coins['emd_scrypt'] =  Coin('EmeraldScrypt')
coins['elc_scrypt'] =  Coin('Elacoin')
coins['elp_scrypt'] =  Coin('Elephantcoin')
coins['exc_scrypt'] =  Coin('Extremecoin')
coins['ezc_scrypt'] =  Coin('EZCoin')
coins['frc_sha-256'] =  Coin('Freicoin')
coins['frsh_fresh'] =  Coin('Freshcoin')
coins['frk_scrypt'] =  Coin('Franko')
coins['fst_scrypt'] =  Coin('Fastcoin')
coins['ftc_neoscrypt'] =  Coin('Feathercoin')
coins['gdc_scrypt'] =  Coin('Grandcoin')
coins['glc_scrypt'] =  Coin('Globalcoin')
coins['gld_scrypt'] =  Coin('GLDCoin')
coins['glx_scrypt'] =  Coin('Galaxycoin')
coins['grs_groestl'] =  Coin('Groestlcoin')
coins['hbn_scrypt'] =  Coin('Hobonickel')
coins['hvc_heavycoin'] =  Coin('Heavycoin')
coins['hyc_scrypt'] =  Coin('Hypercoin')
coins['ifc_scrypt'] =  Coin('Infinitecoin')
coins['ixc_sha-256'] =  Coin('IXcoin')
coins['karm_scrypt'] =  Coin('Karmacoin')
coins['kgc_scrypt'] =  Coin('Krugercoin')
coins['jkc_scrypt'] =  Coin('Junkcoin')
coins['lbw_scrypt'] =  Coin('Lebowski')
coins['lky_scrypt'] =  Coin('Luckycoin')
coins['lot_scrypt'] =  Coin('Lottocoin')
coins['ltc_scrypt'] =  Coin('Litecoin')
coins['max_keccak'] =  Coin('Maxcoin')
coins['mec_scrypt'] =  Coin('Megacoin')
coins['mem_scrypt'] =  Coin('Memecoin')
coins['mnc_scrypt'] =  Coin('Mincoin')
coins['moon_scrypt'] = Coin('Mooncoin')
coins['myr_skein'] = Coin('MyriadcoinSkein')
coins['myr_qubit'] = Coin('MyriadcoinQubit')
coins['myr_groestl'] = Coin('MyriadcoinGroestl')
coins['myr_scrypt'] = Coin('MyriadcoinScrypt')
coins['nec_scrypt'] =  Coin('Neocoin')
coins['nbl_scrypt'] =  Coin('Nibble')
coins['nlg_scrypt'] =  Coin('Guldencoin')
coins['nrb_scrypt'] =  Coin('NoirBits')
coins['nvc_scrypt'] =  Coin('NovaCoin')
coins['orb_neoscrypt'] =  Coin('OrbitcoinNeoscrypt')
coins['orb_scrypt'] =  Coin('OrbitcoinScrypt')
coins['osc_sha-256'] =  Coin('OpenSourceCoin')
coins['phs_scrypt'] =  Coin('Philosopherstone')
coins['ppc_sha-256'] =  Coin('PPCoin')
coins['pot_scrypt'] =  Coin('PotCoin')
coins['pwc_scrypt'] =  Coin('Powercoin')
coins['pxc_neoscrypt'] =  Coin('Phoenixcoin')
coins['rpc_scrypt'] =  Coin('RonPaulCoin')
coins['ryc_scrypt'] =  Coin('RoyalCoin')
coins['sbc_scrypt'] =  Coin('StableCoin')
coins['spt_scrypt'] =  Coin('Spot')
coins['start_x11'] =  Coin('Startcoin')
coins['sxc_scrypt'] =  Coin('Sexcoin')
coins['tgc_sha-256'] =  Coin('TigerCoin')
coins['tips_scrypt'] = Coin('FedoraCoin')
coins['trc_sha-256'] =  Coin('TerraCoin')
diffsrc['trc_sha-256'] = 'http://cryptocoinexplorer.com:3750/chain/Terracoin/q/getdifficulty'
coins['uno_sha-256'] =  Coin('Unobtainium')
coins['uro_x11'] =  Coin('Urocoin')
coins['via_scrypt'] =  Coin('Viacoin')
# coins['utc_Chacha (Nf15)'] =  Coin('Ultracoin')   Problematic algorithm name...has a space
coins['vtc_lyra2re'] =  Coin('Vertcoin')
coins['vnl_whirlpoolx'] =  Coin('Vanillacoin')
coins['vmc_scrypt-n'] =  Coin('Virtualmining')
coins['wdc_scrypt'] =  Coin('Worldcoin')
coins['xdn_cryptonight'] =  Coin('duckNote')
coins['xjo_sha-256'] =  Coin('Joulecoin')
coins['xmr_cryptonight'] =  Coin('Monero')
# coins['yac_Chacha (Nf16)'] =  Coin('YaCoin')     Problematic algo name
coins['zet_sha-256'] =  Coin('Zetacoin')

# Merged
coins['dvc_sha-256'] = Coin('Devcoin')
coins['dvc_sha-256'].merged = True
coins['ixc_sha-256'] = Coin('IXCoin')
coins['ixc_sha-256'].merged = True
coins['i0c_sha-256'] =  Coin('I0Coin')
coins['i0c_sha-256'].merged = True
coins['nmc_sha-256'] =  Coin('NameCoin')
coins['nmc_sha-256'].merged = True
# Kind of an alternate coin...
coins['vanity'] = Coin('Vanity Mining')

#print coins.keys()

# Read in config file
Config = ConfigParser.ConfigParser()
Config.read('./cryptoSwitcher.config')

# Enable the coins you want to mine here.
for key in coins:
    try:
        coins[key].willingToMine = Config.getboolean('MineCoins', 'mine' + key)
    except:
        continue

# You should have scripts that stop all other forms of mining, set
# your clocks and environment variables appropriately, and start
# mining the appropriate coin. I have these called 'litecoin.sh',
# 'bitcoin.sh' etc., but edit and/or replace these as you see fit.

# Any coins you aren't mining you can just leave blank.
for key in coins:
    try:
        coins[key].command = Config.get('Scripts', key + 'script')
    except:
        continue

# Read source list
try:
    source = [x.strip() for x in Config.get('Data-Source','source').split(',')]
except:
    try:
        source = [x.strip() for x in Config.get('Misc','source').split(',')]
        print "Warning: you are using an old config file structure. please update using the config sample file."
    except:
        sys.exit("ERROR: Cannot read source from config file.")


# Read source list
try:
    source_cryptoswitcher = [x.strip() for x in Config.get('Data-Source','source_cryptoswitcher').split(',')]
except:
    source_cryptoswitcher = ''
    print "Warning: couldnt read source_cryptoswitcher from config file. Leaving blank."

# Read hashrates
try:
    hashrate_sha256 = int(Config.get('Data-Source','hashrate_sha256'))
    hashrate_scrypt = int(Config.get('Data-Source','hashrate_scrypt'))
    # MORE hashrates for new algorithms
    # hashrate_skein = int(Config.get('Data-Source','hashrate_skein'))
    # hashrate_qubit = int(Config.get('Data-Source','hashrate_qubit'))
    # hashrate_groestl = int(Config.get('Data-Source','hashrate_myriad'))
    # hashrate_x11 = int(Config.get('Data-Source','hashrate_x11'))
except:
    hashrate_sha256 = 1000
    hashrate_scrypt = 1
    # hashrate_skein = ?
    # hashrate_qubit = ?
    # hashrate_groestl = ?
    # hashrate_x11 = ?
    print "Warning: couldnt read hashrates from config file. Setting to 1:1000."

# Get idle time between two profitability check cycles
try:
    idletime = int(Config.get('Misc','idletime'))
except:
    idletime = 5
    print "Warning: couldnt read idletime from config file. Setting to 5 min."

# Get the coinfees
for key in coins:
    try:
        coins[key].fee = float(Config.get('Fees','fee'+key))
    except:
        continue


# And now some information to calculate Vanity Address mining profitability
try:
    gkeypersec = float(Config.get('Misc','gkeypersec')) #Gigakeys per second you can test
    ghashpersec = float(Config.get('Misc','ghashpersec')) #Gigahash per second you can output doing normal BTC mining.
except:
    print "Warning: couldnt read gkeypersec and ghashpersec from config file."

# If you want to sell your coins on BTCE ASAP, then there's a bit more setup for you
try:
    enableBittrex = Config.getboolean('Sell','enableBittrex')
    enableBTCE = Config.getboolean('Sell','enableBTCE')     # Delete this line when Bittrex works fine
    # print "enableBTCE=", enableBTCE
    enableVircurex = Config.getboolean('Sell','enableVircurex')
    enableCryptsy = Config.getboolean("Sell", "enableCryptsy")
    vircurexSecret = Config.get('Sell','vircurexSecret')
    vircurexUsername = Config.get('Sell','vircurexUsername')
    cryptsyPubkey = Config.get("Sell", "cryptsyPublicKey")
    cryptsyPrivkey = Config.get("Sell", "cryptsyPrivateKey")
except:
    enableBTCE = False
    enableBittrex = False
    enableVircurex = False
    enableCryptsy = False
    print "Warning: couldnt read sell information from config file. Disabling auto sell."

# And flag which coins you want to sell as they come in. These coins will only
# sell for BTC, not for USD or any other cryptocoin.
for key in coins:
    try:
        coins[key].willingToSell = Config.getboolean('Sell','sell'+key)
    except:
        continue

#Trade multiplier. i.e. Don't sell for the highest current bid if this is
#larger than 1, but make a new ask at highest_bid * tradeMultiplier.

tradeMultiplier = 1
try: 
    tradeMultiplier = float(Config.get('Misc','tradeMultiplier'))
except:
    pass

tradeMultiplierCheck = False
try:
    tradeMultiplierCheck = Config.getboolean ('Misc', 'tradeMultiplierCheck')
except:
    pass

# SELL COINS AT BITTREX 
# def sellCoinBittrex(coin, tradeapi):
#     try:
#         balance = bittrex.get_balance(currency)
#     except:
#         # probably a coin that BTCE doesn't have an exchange for, so just return
#         return
#     if balance > 0.1:
#         # i.e. if we're selling and we have some to sell that's larger than the minimum order...
#         asks, bids = bittrex.getDepth(coin + '_btc')
#         price = bids[0][0]*tradeMultiplier
#         if price > asks[0][0] and tradeMultiplierCheck == True:
#             price = asks[0][0] - 0.00000001
#         tr = tradeapi.trade(coin + '_btc', 'sell', price, balance)
        # If tradeMultiplier is 1, then this sells at the highest price someone
        # currently has a bid lodged for.  It's possible that this won't
        # totally deplete our reserves, but any unsold immediately will be left
        # on the book, and will probably sell shortly.
        # A higher trade multiplier than 1 will not sell right away, but will
        # leave an order on the book.

def sellCoinBTCE(coin, tradeapi):
    r = tradeapi.getInfo()
    # print "r=",r
    try:
        balance = getattr(r, 'balance_' + coin)
        # print "balance=",balance
    except:
        # Probably a coin that BTCE doesn't have an exchange for, so just return
        return
    if balance > 0.1:
        # i.e. if we're selling and we have some to sell that's larger than the minimum order...
        asks, bids = btceapi.getDepth(coin + '_btc')
        # print "asks,bids=",asks, bids
        price = bids[0][0]*tradeMultiplier
        if price > asks[0][0] and tradeMultiplierCheck == True:
            price = asks[0][0] - 0.00000001
        tr = tradeapi.trade(coin + '_btc', 'sell', price, balance)
        # print "tr =",tr
        # If tradeMultiplier is 1, then this sells at the highest price someone
        # currently has a bid lodged for.  It's possible that this won't
        # totally deplete our reserves, but any unsold immediately will be left
        # on the book, and will probably sell shortly.
        # A higher trade multiplier than 1 will not sell right away, but will
        # leave an order on the book.

def sellCoinVircurex(coin):
    pair = vircurexapi.Pair(coin+'_btc')
    try:
        bid = pair.highest_bid
        ask = pair.lowest_ask
    except:
        # Probably a coin that Vircurex doesn't have an exchange for, so just return
        return
    account = vircurexapi.Account(vircurexUsername, vircurexSecret)
    balance = account.balance(coin.upper())
    if balance >= 0.1:
        price = bid * tradeMultiplier
        if price > ask and tradeMultiplierCheck == True:
            price = ask - 0.00000001
        order = account.sell(coin.upper(),balance, 'BTC', price)
        account.release_order(order['orderid'])

def sellCoinCryptsy(coin):
    acct = PyCryptsy(cryptsyPubkey, cryptsyPrivkey)
    bal = acct.GetAvailableBalance(coin)
    price = acct.GetBuyPrice(coin, "BTC")*tradeMultiplier
    sell = acct.GetSellPrice(coin, "BTC")
    if price > sell and tradeMultiplierCheck == True:
        price = sell - 0.00000001
    if price > 0:
        acct.CreateSellOrder(coin, "BTC", bal, price)
    else:
        #Try to get price from a coin/LTC market:
        priceltc = acct.GetBuyPrice(coin, "LTC")*tradeMultiplier
        if priceltc > 0:
            acct.CreateSellOrder(coin, "LTC", bal, priceltc)
    return


# The following code will be obsolete when we substitute BTCe with Bittrex.
#if enableBTCEw:
if enableBTCE:
    key_file = './key'
    handler = btceapi.KeyHandler(key_file)
    #print "handler= ", handler
    key = handler.keys.keys()[0]
    #print "key=",key
    secret, nonce =  handler.keys[handler.keys.keys()[0]]
    #print "secret, nonce=", secret, nonce
    authedAPI = btceapi.TradeAPI(key, secret, nonce)
    #print "authedAPI=", authedAPI

# MORE WORK HERE !
# if enableBittrex:
#     key_file = './key'
#     handler = bittrex.KeyHandler(key_file)
#     key = handler.keys.keys()[0]
#     secret, nonce =  handler.keys[handler.keys.keys()[0]]
#     authedAPI = bittrex.Bittrex(nonce, key, secret)

# Create http handler
opener = urllib2.build_opener()
opener.addheaders = [('User-agent', 'CryptoSwitcher')]

# Disable extended status output (=> coin price and difficulty) by default. only
# enable it, if at least one coins profitability is calculated by cryptoswitcher
extout = False

# MAIN LOOP----------------------------------------------------------------------------------------------------------------------------
cnt_all = 0
while True:
    # Print header
    print "\n\n\n<<< Round %d >>>" % (cnt_all+1)
    print "time:", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    # Get data from sources
    prestr = "Getting data... "

    for x in source:
        # Whattomine
        if x== 'whattomine' or x== 'cryptoswitcher':
             try:
                 fullstr = prestr + "whattomine"
                 print fullstr + (79-len(fullstr))*" " + "\r",                 
                 url = "http://www.whattomine.com/coins.json"
                 data_wm = requests.get(url).json()                                
             except:
                 pass

        if x== 'coinchoose':
            try:
                fullstr = prestr + "coinchoose"
                print fullstr + (79-len(fullstr))*" " + "\r",
                req = urllib2.Request("http://www.coinchoose.com/api.php")
                opener_cc = urllib2.build_opener()
                opener_cc.addheaders = [('User-agent', 'CryptoSwitcher')]
                f = opener_cc.open(req, timeout = 5)
                data_cc = simplejson.load(f)                
            except:
                pass         

        # Dustcoin
        elif x== 'dustcoin':
            try:
                fullstr = prestr + "dustcoin"
                print fullstr + (79-len(fullstr))*" " + "\r",
                usock = urllib2.urlopen('http://dustcoin.com/mining', timeout = 5)
                data = usock.read()
                usock.close()
                soup = BeautifulSoup(data)
                table_dustcoin = soup.findAll('tr',{ "class":"coin" })
                #print table_dustcoin
            except:
                pass

        # Coinotron
        # elif x== 'coinotron':
        #     try:
        #         fullstr = prestr + "coinotron"
        #         print fullstr + (79-len(fullstr))*" " + "\r",
        #         usock = urllib2.urlopen('https://coinotron.com/coinotron/AccountServlet?action=home', timeout = 5)
        #         data = usock.read()
        #         usock.close()
        #         soup = BeautifulSoup(data)
        #         table_coinotron = soup.findAll('tr')
        #         #print table_coinotron
        #     except:
        #         pass


    for x in source_cryptoswitcher:
        # Cryptsy
        # We keep this snippet instead of the BeautifulSoup because we need the data_cyp variable later on
        # And because it works (?)
          if x== 'cryptsy':
              try:
                  fullstr = prestr + "cryptsy"
                  print fullstr + (79-len(fullstr))*" " + "\r",
                  req = urllib2.Request("http://pubapi.cryptsy.com/api.php?method=orderdata")
                  opener_cyp = urllib2.build_opener()
                  opener_cyp.addheaders = [('User-agent', 'CryptoSwitcher')]
                  f = opener_cyp.open(req, timeout = 5)
                  data_cyp = simplejson.load(f)            
              except:
                  pass        

    # Assign data to coins
    # Loop through coins
    for abbreviation, c in coins.items():        
        # Only get profitability for coins which we are interested in.
        # This saves network traffic and running time

        if c.willingToMine == False:
            continue

        success = 0
        # Loop trough source list. try first entry first.
        for x in source:
            if x == 'whattomine':
                try:
                    for key, value in data_wm['coins'].items():
                        if value['tag'].lower() + "_" + value['algorithm'].lower() == abbreviation:
                            # coins[value['tag'].lower()].ratio = float(value['profitability'])                            
                            coins[abbreviation].ratio = float(value['profitability'])                            
                            # coins[value['tag'].lower()].source = 'wm'
                            coins[abbreviation].source = 'wm'
                            success = 1
                            break
                except:
                    continue

            # We keep it to update Bitcoin values.
            if x == 'coinchoose':
                 try:
                     for item in data_cc:
                        name = item['symbol'].lower() + "_"  + item['algo'].lower()
                        if  name == abbreviation:

                             # coins[item['symbol'].lower()].ratio = float(item['adjustedratio'])
                            coins[abbreviation].ratio = float(item['adjustedratio'])
                             # coins[item['symbol'].lower()].source = 'cc'
                            coins[abbreviation].source = 'cc'
                            success = 1
                            break
                 except:
                     continue

            elif x == 'dustcoin':
                try:
                    i=0
                    for coinrow in table_dustcoin:
                        coinName, profit = coinrow.find('strong',text=True).text, coinrow.find('td',{"id":"profit"+str(i)}).text.replace('%','')
                        # make sure the profit we read is floating value, if not, continue loop and keep old profit value until next check
                        try:
                            profit = float(profit)
                        except:
                            continue
                        # locate algorithm    
                        algorithm = coinrow.find('td',{"id":"algo0"},'small')
                        #print algorithm
                        # calculate profitabilty
                        if coinName + "_" + algorithm == coins[abbreviation].name:
                            coins[abbreviation].ratio = float(profit)
                            coins[abbreviation].source = 'dc'
                            success = 1
                            break
                        i+=1
                except:
                    continue

            # elif x == 'coinotron':
            #     try:
            #         i = 0
            #         for coinrow in table_coinotron:
            #             coinName = coinrow.findNext('td').contents[0]
            #             profit = coinrow.findNext('td').findNext('td').findNext('td').findNext('td').findNext('td').findNext('td').findNext('td').findNext('td').contents[0]
            #             # when all coins where read, leave loop
            #             if i == 5: break
            #             i += 1
            #             # convert profitability to percent
            #             profit = float(profit)*100
            #             # calculate profitabilty
            #             if coinName.lower() == abbreviation:
            #                 coins[abbreviation].ratio = float(profit)
            #                 coins[abbreviation].source = 'ct'
            #                 success = 1
            #                 break
            #             i+=1
            #     except:
            #         continue

            # PROFITABILITY CALCULATION !!!-------------------------------------------------------------------------------------------
            # A) Collecting data for difficulty and block reward.
            elif x =='cryptoswitcher':
                # Get difficulty and block rewards
                # Source for difficulty data depends on coin
                try:
                    # If this is the first time we come here, update btc as well.
                    # Otherwise we are unable to calculate the profitability.
                    fullstr = prestr + "difficulty of " + coins[abbreviation].name
                    print fullstr + (79-len(fullstr))*" " + "\r",
                    if coins['btc_sha-256'].reward == 0:
                        # DOES THIS WORK? Updating Bitcoin from Coinchoose.
                        for item in data_cc:
                            if item['symbol'].lower() + "_" + item['algo'] == 'btc_sha-256':
                                coins['btc_sha-256'].diff = float(item['difficulty'])
                                coins['btc_sha-256'].reward = float(item['reward'])
                                break

                    # Get difficulty values from whattomine by default                    
                    for key, value in data_wm['coins'].items():
                        if value['tag'].lower() + "_" + value['algorithm'].lower() == abbreviation:
                            #coins[value['tag'].lower()].diff = float(value['difficulty'])
                            # Simpler:
                            coins[abbreviation].diff = float(value['difficulty'])                            
                            #coins[value['tag'].lower()].reward = float(value['block_reward'])
                            # Simpler:
                            coins[abbreviation].reward = float(value['block_reward'])
                            #coins[value['tag'].lower()].algo = value['algorithm']
                            # Simpler:
                            coins[abbreviation].algo = value['algorithm']

                    # if we dont have a difficulty source for our coin, continue loop and get profitabilty from
                    # other sources                    
                    if item['symbol'].lower() + "_" + item['algo'] != abbreviation:
                       continue

                    # for btg/trc: use different sources for difficulty
                    if diffsrc[abbreviation] != '':
                        req = urllib2.Request(diffsrc[abbreviation])
                        f = opener.open(req, timeout = 5)
                        coins[abbreviation].diff = simplejson.load(f)

                    # For btc: we dont need to calculate
                    # That's why the ratio of BTC is always 100 !!!
                    if abbreviation == 'btc':
                        coins['btc_sha-256'].ratio = 100.0
                        coins['btc_sha-256'].source = '--'
                        coins['btc_sha-256'].algo = "SHA-256"
                        coins['btc_sha-256'].price = 1.0
                        success = 1
                        break

                    # More coins here: DBG, DGC, MYR
                    # ???
                    # More algos: skein, qubit, myriad-groestl, x11
                    # ???
                except:
                    continue


                # B) Calculate highest buy value
                # Use only data sources defined in source_cryptoswitcher
                coins[abbreviation].price = 0.0
                for y in source_cryptoswitcher:

                    # if coin profitability couldnt be processed manually in the
                    # last round, then they are probably not traded on the chosen
                    # markets. so the coin is removed from manual processing.
                    if coins[abbreviation].source != '--' and coins[abbreviation].source != 'cs':
                        continue
                    
                    # MUCH MORE WORK HERE
                    # bittrex
                    # if y=='bittrex':
                    #     try:
                    #         fullstr = prestr + "price of " + coins[abbreviation].name + " at BTC-E"
                    #         print fullstr + (79-len(fullstr))*" " + "\r",
                    #         req = urllib2.Request("https://btc-e.com/api/2/" + abbreviation + "_btc/ticker")
                    #         f = opener.open(req, timeout = 5)
                    #         output = simplejson.load(f)
                    #         if coins[abbreviation].price < float(output['ticker']['sell']):
                    #             coins[abbreviation].price = float(output['ticker']['sell'])
                    #     except:
                    #         continue        

                    # Soon to be obsolete and replaced with the above code for Bittrex.
                    # btc-e
                    if y == 'btce':
                        try:
                            fullstr = prestr + "price of " + coins[abbreviation].name + " at BTC-E"
                            print fullstr + (79-len(fullstr))*" " + "\r",
                            req = urllib2.Request("https://btc-e.com/api/2/" + abbreviation + "_btc/ticker")
                            f = opener.open(req, timeout = 5)
                            output = simplejson.load(f)
                            if coins[abbreviation].price < float(output['ticker']['sell']):
                                coins[abbreviation].price = float(output['ticker']['sell'])
                        except:
                            continue

                    # bter
                    elif y == 'bter':
                        try:
                            fullstr = prestr + "price of " + coins[abbreviation].name + " at Bter"
                            print fullstr + (79-len(fullstr))*" " + "\r",
                            req = urllib2.Request("https://bter.com/api/1/ticker/" + abbreviation + "_btc")
                            f = opener.open(req, timeout = 5)
                            output = simplejson.load(f)
                            if coins[abbreviation].price < float(output['buy']):
                                coins[abbreviation].price = float(output['buy'])
                        except:
                            continue

                    # vircurex
                    elif y == 'vircurex':
                        try:
                            fullstr = prestr + "price of " + coins[abbreviation].name + " at Vircurex"
                            print fullstr + (79-len(fullstr))*" " + "\r",
                            req = urllib2.Request("https://vircurex.com/api/get_highest_bid.json?base=" + abbreviation + "&alt=btc")
                            f = opener.open(req, timeout = 5)
                            output = simplejson.load(f)
                            if coins[abbreviation].price < float(output['value']):
                                coins[abbreviation].price = float(output['value'])
                        except:
                            continue

                    # cryptsy
                    elif y == 'cryptsy':
                        try:
                            fullstr = prestr + "price of " + coins[abbreviation].name + " at Cryptsy"
                            print fullstr + (79-len(fullstr))*" " + "\r",
                            for item in data_cyp['return']:
                                if item.lower()==abbreviation:
                                    if data_cyp['return'][item]['secondarycode']=='BTC':
                                        if coins[abbreviation].price < float(data_cyp['return'][item]['buyorders'][0]['price']):
                                            coins[abbreviation].price = float(data_cyp['return'][item]['buyorders'][0]['price'])
                                    success = 1
                                    break
                        except:
                            continue

                # C) Calculate profitability
                if coins[abbreviation].price != 0.0:
                    try:
                        if coins[abbreviation].algo == 'scrypt':
                            coins[abbreviation].ratio = (coins[abbreviation].reward/coins[abbreviation].diff)/(coins['btc_sha-256'].reward/coins['btc_sha-256'].diff)*coins[abbreviation].price*100/(hashrate_sha256/hashrate_scrypt)
                        else:
                            coins[abbreviation].ratio = (coins[abbreviation].reward/coins[abbreviation].diff)/(coins['btc_sha-256'].reward/coins['btc_sha-256'].diff)*coins[abbreviation].price*100
                        coins[abbreviation].source = 'cs'
                        success = 1

                        # at least one coins profitability was calculated by cryptoswitcher
                        # => enable extended status output
                        extout = True
                        break
                    except:
                        continue

            if success==1:
                break


    # Now work out how profitable BTC mining really is, if we're doing any merged mining
    if coins['nmc_sha-256'].willingToMine:
        coins['btc_sha-256'].ratio += coins['nmc_sha-256'].ratio
    if coins['dvc_sha-256'].willingToMine:
        coins['btc_sha-256'].ratio += coins['dvc_sha-256'].ratio
    if coins['ixc_sha-256'].willingToMine:
        coins['btc_sha-256'].ratio += coins['ixc_sha-256'].ratio

    fullstr = prestr + "done"
    print fullstr + (79-len(fullstr))*" " + "\r"


    # Now get data for vanity mining
    if coins['vanity'].willingToMine:
        vanityDataValid = True
        try:
            usock = urllib2.urlopen('http://www.fizzisist.com/mining-value/api/bitcoin-value',timeout=1)
            btcperghash = usock.read()
            usock.close()
            btcperghash = float(btcperghash)
        except (urllib2.URLError, ValueError, socket.timeout) as  e:
            print "There was an error: ,", e
            vanityDataValid = False

        try:
            usock = urllib2.urlopen('http://www.fizzisist.com/mining-value/api/vanitypool-value',timeout=1)
            btcpergkey = usock.read()
            btcpergkey = float(btcpergkey)
            usock.close()
        except (urllib2.URLError, ValueError, socket.timeout) as  e:
            print "There was an error: ,", e
            vanityDataValid = False

        if vanityDataValid:
            # Now put vanity mining in terms of BTC mining.
            vanitybtcsec = gkeypersec * btcpergkey
            miningbtcsec = ghashpersec * btcperghash
            vanityprof = vanitybtcsec / miningbtcsec * 100
            coins['vanity'].ratio = vanityprof
            # print 'Vanity Mining', vanityprof

    # BEST COIN---------------------------------------------------------------------------------------------------        
    # Now find the best profit coin
    bestcoin = 'btc_sha-256'
    bestprof = 0
    print "Comparing profitability..."
    print "-"*36
    for abbreviation, c in coins.items():
        if c.willingToMine:
            print "%11s: %3d  (fee: %2d, src: %s)" % (coins[abbreviation].name, c.ratio, coins[abbreviation].fee, coins[abbreviation].source),
            if extout == True:
                if coins[abbreviation].source == "cs" or abbreviation == "btc_sha-256":
                # Uncomment if you want to see price/algo/diff values when the source is wm or cc.    
                #if coins[abbreviation].source == "cs" or coins[abbreviation].source == "wm" or coins[abbreviation].source == "cc" or abbreviation == "btc":
                    print "(pr: %.5f, di[%s]: %.2f)" % (coins[abbreviation].price, coins[abbreviation].algo, coins[abbreviation].diff),
                else:
                    # If diff is valid print it
                    if coins[abbreviation].reward != 0:
                        print "(pr:  -NA-  , di[%s]: %.2f)" % (coins[abbreviation].algo, coins[abbreviation].diff),
                    else:
                        print "(pr:  -NA-  , di[%s]:  -NA-  )" % (coins[abbreviation].algo),
            print ""

        if c.ratio - coins[abbreviation].fee > bestprof and c.willingToMine:
            bestcoin = abbreviation
            bestprof = c.ratio - coins[abbreviation].fee
    print "-"*36
    print "=> Best: %d, mining %s" % (bestprof, coins[bestcoin].name)
    coins[bestcoin].median = ((coins[bestcoin].median * coins[bestcoin].cnt) + coins[bestcoin].ratio-coins[bestcoin].fee) / (coins[bestcoin].cnt + 1)
    coins[bestcoin].cnt = coins[bestcoin].cnt + 1


    if coins[bestcoin].miningNow == False:
        # i.e. if we're not already mining the best coin
        print '=> Switching to %s (running %s)' % (coins[bestcoin].name, coins[bestcoin].command)
        for abbreviation, c in coins.items():
            c.miningNow = False
        coins[bestcoin].miningNow = True
        subprocess.Popen(coins[bestcoin].command, shell = True)

    # Sell some coins if that's what we're into
    for abbreviation, c in coins.items():
        # if c.willingToSell and (c.miningNow or c.merged) and enableBTCE:
        if c.willingToSell and (c.miningNow or c.merged) and enableBittrex:    
            # i.e. if we're willing to sell it AND it's still worth more than BTC -
            # with pool payout delays and wild exchange swings, while it might be
            # profitable to have mined it, we didn't sell it quickly enough. This
            # keeps hold of the coin until you've made a decision.
            sellCoinBTCE(abbreviation, authedAPI)
            # sellCoinBittrex(abbreviation, authedAPI)
        # elif c.willingToSell and c.miningNow and enableVircurex:
        if c.willingToSell and enableVircurex and (c.miningNow or c.merged):
            sellCoinVircurex(abbreviation)
        if c.willingToSell and enableCryptsy and (c.miningNow or c.merged):
            sellCoinCryptsy(abbreviation)

    # ...and now save the keyfile in case the script is aborted.
    if enableBTCE:
        handler.setNextNonce(key,time.time()) #Thanks, jsorchik
        handler.save(key_file)

    # if enableBittrex:
    #     handler.setNextNonce(key,time.time()) #Thanks, jsorchik
    #     handler.save(key_file)        

    # Create status output strings
    sname = "#        "
    smedian = "# Median:"
    stime = "# Time:  "
    median_all = 0
    cnt_all = 0
    for abbreviation, c in coins.items():
        if c.willingToMine and (not c.merged):
            coins[abbreviation].h, coins[abbreviation].m = divmod(coins[abbreviation].cnt*idletime, 60)
            if coins[abbreviation].h < 10:
                sname += "%5s  " % (abbreviation.upper())
                smedian += "%5d |" % (coins[abbreviation].median)
                stime += "%2d:%02d |" % (coins[abbreviation].h, coins[abbreviation].m)
            elif coins[abbreviation].h < 100:
                sname += "%6s  " % (abbreviation.upper())
                smedian += "%6d |" % (coins[abbreviation].median)
                stime += "%3d:%02d |" % (coins[abbreviation].h, coins[abbreviation].m)
            else:
                sname += "%7s  " % (abbreviation.upper())
                smedian += "%7d |" % (coins[abbreviation].median)
                stime += "%4d:%02d |" % (coins[abbreviation].h, coins[abbreviation].m)
            if coins[abbreviation].cnt > 0:
                median_all = ((median_all * cnt_all) + (coins[abbreviation].median*coins[abbreviation].cnt)) / (cnt_all+coins[abbreviation].cnt)
                cnt_all += coins[abbreviation].cnt

    # remove last chars
    sname = sname[:-2]
    smedian = smedian[:-2]
    stime = stime[:-2]

    smedian_all = '# Total Median:%5d' % (median_all)
    stime_all = '# Total Time:%4d:%02d' % (divmod(cnt_all*idletime, 60))

#    # fill strings to screen width and add "#" to the end
#    sname = "%s%s%s" % (sname, " "*(79-len(sname)), "#")
#    smedian = "%s%s%s" % (smedian, " "*(79-len(smedian)), "#")
#    stime = "%s%s%s" % (stime, " "*(79-len(stime)), "#")
#    smedian_all = "%s%s%s" % (smedian_all, " "*(79-len(smedian_all)), "#")
#    stime_all = "%s%s%s" % (stime_all, " "*(79-len(stime_all)), "#")

    # output status strings
#    print "\n", "#"*80+sname+smedian+stime+smedian_all+stime_all+"#"*80
    print "\n", sname
    print smedian
    print stime
    print smedian_all
    print stime_all, "\n"

    # sleep
    print 'Going to sleep...'
    i=0
    while i < idletime*60:
        print "Seconds remaining:", (idletime*60-i), " ", "\r",
        time.sleep(1)
        i += 1
