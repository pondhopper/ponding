# test poloniex
#!/usr/bin/env python
from bs4 import BeautifulSoup
import urllib2
import time
import subprocess
import numpy as np
import sys

# Ugly hack so git submodule init is adequate.
sys.path.insert(0, './btce-api/')
import btceapi
sys.path.insert(0, './pyvircurex/')
import vircurex as vircurexapi
sys.path.insert(0, './PyCryptsy/')
from PyCryptsy import PyCryptsy
sys.path.insert(0, './python-bittrex/')
import bittrex

import ConfigParser
import simplejson
import socket
import requests

# COINS
class Coin:
    def __init__(self, name):
        self.ratio = 0 # assume totally unprofitable unless otherwise shown to be the case.
        self.willingToMine = False
        self.miningNow = False
        self.merged = False
        self.willingToSell = False
        self.command = '' # the command that is run when we want to mine this coin.
        self.name = name
        self.cnt = 0
        self.median = 0
        self.m = 0
        self.h = 0
        self.fee = 0
        self.source = '--'
        self.price = 0.0
        self.diff = 9999999999.9 # start off with ridiculously high diff so we dont mine the wrong coin
        self.reward = 0
        self.algo = ""

coins = {}      # Creating an empty dictionary for the coins
diffsrc = {}    # Creating an empty dictionary for btg and trc

# Bitcoin, our reference coin
coins['btc_sha-256'] = Coin('Bitcoin')

# Coins that we're likely to have nodes
coins['dgb_skein'] = Coin('DigitalbyteSkein')
coins['dgb_groestl'] = Coin('DigitalbyteGroestl')
coins['dgb_qubit'] = Coin('DigitalbyteQubit')

coins['dgc_x11'] =  Coin('DigitalCoinX11')

coins['myr_skein'] = Coin('MyriadcoinSkein')
coins['myr_qubit'] = Coin('MyriadcoinQubit')
coins['myr_groestl'] = Coin('MyriadcoinGroestl')

coins['dash_x11'] =  Coin('Dash')

# Other coins with a decent hashrate and high average % profitability
coins['start_x11'] =  Coin('Startcoin')
coins['geo_qubit'] = Coin('Geoqubit')

# Possible coins for a scrypt version
coins['dgb_scrypt'] = Coin('DigitalbyteScrypt')
coins['dgc_scrypt'] =  Coin('DigitalCoinScrypt')
coins['myr_scrypt'] = Coin('MyriadcoinScrypt')
coins['ltc_scrypt'] =  Coin('Litecoin')
coins['tips_scrypt'] = Coin('FedoraCoin')
coins['fst_scrypt'] =  Coin('Fastcoin')
coins['wdc_scrypt'] =  Coin('Worldcoin')

# Merged coins by algo
coins['uis_qubit'] = Coin('UnitusCoinQubit') # Will be mined from Poloniex
coins['uis_qubit'].merged = True
coins['uis_skein'] = Coin('UnitusCoinSkein')
coins['uis_skein'].merged = True
coins['uis_x11'] = Coin('UnitusCoinX11')
coins['uis_x11'].merged = True

coins['doge_scrypt'] = Coin('DogeCoinScrypt')
coins['doge_scrypt'].merged = True

coins['via_scrypt'] =  Coin('Viacoin')
coins['via_scrypt'].merged = True
coins['sys_scrypt'] =  Coin('SysCoin')
coins['sys_scrypt'].merged = True
coins['ultc_scrypt'] =  Coin('UmbrellaLTC')
coins['ultc_scrypt'].merged = True
coins['pst_scrypt'] =  Coin('PesetaCoin')
coins['pst_scrypt'].merged = True
coins['gre_scrypt'] =  Coin('GreenCoin')
coins['gre_scrypt'].merged = True
coins['tco_scrypt'] =  Coin('TacoCoin')
coins['tco_scrypt'].merged = True

for abbreviation, c in coins.items():
    opener = urllib2.build_opener()
    try:
        url = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-" + abbreviation[0:3]
        req = urllib2.Request(url)
        f = opener.open(req, timeout = 5)
        output = simplejson.load(f)
        print output["result"][0]['Last']
        # polocoins = "BTC_" + (abbreviation[0:3]).upper()
        # print abbreviation + ": " + output[polocoins]['last']
    except TypeError:
         # print abbreviation + ": " +"doesn't exist on Bittrex"
         continue