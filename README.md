Pondhopper
==============

A script to switch between mining BTC and altcoins as profitability dictates,
by using Whattomine, Coinchoose, Dustcoin, Cryptsy, Bittrex, Poloniex, Bter, Vircuvex as sources of information.

Dependencies
---
* NumPy
* BeautifulSoup4
* urllib2
* simplejson
* requests
* bittrex-api (included)
* pyvircurex (included)
* PyCryptsy (included)
* cgminer (optional)

Usage
---
Edit the scripts such as `bitcoin.sh` so that they start mining the appropriate
coins with your desired settings. I recommend the scripts start the appropriate
miner inside a `screen` session, which the two example scripts I've included
will do. Then poke around in `pondhopper.config.sample to set everything
up, including which coins you wish to try and mine or merged mine. Then run
    
    git submodule init
    git submodule update

to download the Bittrex, Vircurex, and Cryptsy python APIs. Then rename 
`pondhopper.config.sample` to `pondhopper.config` and run
 
    python pondhopper.py

The script should work out what the best mining option is, and run the
appropriate script. It checks every hour for a change in the situation, and if
the options is enabled it will also sell any coins that have been
auto-withdrawn to BTC-E.  I bear no responsibility for any losses you incur by
using this script to sell your cryptocoins. If you wish to use these options,
set the auto-withdrawal limits at your pools as low as they will go to limit
your exposure to a changing exchange rate. You will also need to add your BTC-E
API key and secret to the file `key.sample` and rename it to `key`.


Thanks
---

To the guys that made CryptoSwitcher. Our work is heavily based on yours, so you saved us time from creating the whole thing from scratch.


